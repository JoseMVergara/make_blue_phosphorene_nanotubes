# Make_blue_phosphorene_nanotubes

Python script to build blue-phosphorene nanotubes. 

## Requeriments:
    numpy
    math

## Execution:
>python Make_nanotube.py

## Example:

Blue-Phosphorene nanotubes with $n=14$ and $m = 14$ (Armchair).

__app:__ bond lenght between phosphorene atoms (Ångströms).
        
__a1 and a2:__ unit cell vectors.

__atom1 and atom2:__ initial atoms to be replicated.

__n and  m:__ integers to defined the nanotube quirality.

__verbose:__ if True, plots and prints are displayed.

    app = 1.88
    a2 = ((3/2.)*app,-np.sqrt(3)/2. * app )
    a1 = ((3/2.)*app,np.sqrt(3)/2. * app )

    #initial atoms 

    atom1 = (0.0,0.0,5.0)
    atom2 = (app,0.0,6.3)
    n = 14
    m = 14
    atomsR,unitCell = make_nanotube(app,a1,a2,atom1,atom2,n,m,False)
    writexyz(atomsR,'BPNT1414')