"""
Python script to create blue-phosphorene nanotubes

requeriments:
    numpy
    math

execute:
    >python Make_nanotube.py

see example below.
"""


import numpy as np
from math import atan2, cos, sin, acos, gcd

def mag(x,y):
    """
    Return the magnitude of a vector
    input:
        x: x-coordinates 
        y: y-coordinates
    output:
        rst: Magnitude of a vector 
    """
    rst = 0
    for i in range(len(x)):
        rst += (y[i] - x[i])**2
    return rst**0.5

def writexyz(AtomsR,filename):
    """
    function to write phosphorene nanotube coordinates in xyz file.
    input:
        AtomsR: nanotube atoms coordinates.
        filename: name of resulting xyz file.
    output:
        xyz file.

    """
    myfile = open(f"{filename}.xyz","w")
    myfile.write("{}\n".format(len(AtomsR)))
    myfile.write("xyz\n")
    for i in range(len(AtomsR)):
        myfile.write("{}\t {}\t {}\t {}\t\n".format('P',AtomsR[i][0],AtomsR[i][1],AtomsR[i][2]))
    myfile.close()

def make_nanotube(app,a1,a2,atom1,atom2,n,m,verbose,maxiter=300):
    """
    Create blue-phosphorene nanotube (with buckling)
    inputs:
        app: bond lenght between phosphorene atoms (Ångströms).
        a1 and a2: unit cell vectors.
        atom1 and atom2: initial atoms to be replicated.
        n and  m: integers to defined the nanotube quirality.
        verbose: if True, plots and prints are displayed.
    outputs:
        AtomsR: nanotube coordinates
        unit_cell: nanotube unit cell
    """
    
    #convert inital values to arrays
    a1 = np.array(a1)
    a2 = np.array(a2)
    atom1 = np.array(atom1)
    atom2 = np.array(atom2)

    #we define the chiral vector along which the nanotube is coiled
    Ch = n*a1 + m*a2

    #defined the gcd between (2m+n, 2n+m). 
    num1 = 2*m+n
    num2 = 2*n+m
    dR = gcd(num1,num2)

    #defined the traslation vector of nanotube
    T = num1/dR * a1 -num2/dR * a2

    #The parallelepiped formed by the chiral vector and the 
    #translational vector is the unit cell of the nanotube.
    unitCell = np.linalg.norm(T)

    #replicated atoms to get monolayer
    h = np.linalg.norm(a1)
    Ca = np.cos(30*np.pi/180.)*h
    Co = np.sin(30*np.pi/180.)*h
    atomsx = [atom1[0],atom2[0]]
    atomsy = [atom1[1],atom2[1]]
    atomsz = [atom1[2],atom2[2]]

    for i in range(-maxiter,maxiter):
        mult = np.arange(-i,i+2,2)
        mult = mult[::-1]
        for j in mult:
            x1,y1 = a1[0]+Ca*i,a1[1]+Co*j
            x2,y2 = a2[0]+Ca*i,a2[1]+Co*j

            atomsx += [x1,x2+app]
            atomsy += [y1,y2]
            atomsz += [atom1[2],atom2[2]]

    #get the chiral angle and reciprocal network vectors
    ChT = np.array([maxiter*2.,0])
    TT = np.array([maxiter*2.,0])
    ChI = ChT-Ch
    TI = TT-T
    Chc = np.array([ChI,ChT])
    Tc = np.array([TI,TT])
    Chc2 = np.array([ChI-T,ChT-T])
    Tc2 = np.array([TI-Ch,TT-Ch])

    f = 0.1

    for i in range(len(Chc)):
        if i == 0:
            Chc[i][0],Chc[i][1] = Chc[i][0]-f,Chc[i][1]+f
            Chc2[i][0],Chc2[i][1] = Chc2[i][0]+f,Chc2[i][1]-f
            Tc[i][0],Tc[i][1] = Tc[i][0]+f,Tc[i][1]-f
            Tc2[i][0],Tc2[i][1] = Tc2[i][0]+f,Tc2[i][1]-f
        else:
            Chc[i][0],Chc[i][1] = Chc[i][0]-f,Chc[i][1]+f
            Chc2[i][0],Chc2[i][1] = Chc2[i][0]+f,Chc2[i][1]-f
            Tc[i][0],Tc[i][1] = Tc[i][0]-f,Tc[i][1]+f
            Tc2[i][0],Tc2[i][1] = Tc2[i][0]-f,Tc2[i][1]+f
        
    points = [Chc,Tc,Chc2,Tc2]
    Atoms = []
    angles = []

    vCh = Chc[1]-Chc[0]
    mCh = mag(Chc[0],Chc[1])
    R = mCh/(2*np.pi)

    xCh = vCh[0]
    yCh = vCh[1]
    #anCh = atan2(yCh,xCh)

    for i in range(len(atomsx)):
        point = [atomsx[i],atomsy[i]]
        for j in points:
            l1 = mag(point,j[0])
            l2 = mag(point,j[1]) 
            l3 = mag(j[0],j[1])
            try:
                angle = acos((l3**2 - l1**2 - l2**2)/(-2.*l1*l2))
                angles += [angle]
            except:pass
  
        if abs(np.sum(angles)-2.*np.pi)<1e-3: 
            Atoms += [[atomsx[i],atomsy[i],atomsz[i]]]
        angles = [] 
    
    #cut atoms to be rolled up
    Atoms = np.array(Atoms)


    for i in range(len(Atoms-1)):
        Atoms[i][0] = np.array(Atoms[i][0]) - Chc2[0][0]
        Atoms[i][1] = np.array(Atoms[i][1]) - Chc2[0][1]
        
    if verbose == True:
        xs = []
        ys = []
        for i in range(len(Chc)):
            xs += [Chc[i][0],Tc[i][0],Chc2[i][0],Tc2[i][0]]
            ys += [Chc[i][1],Tc[i][1],Chc2[i][1],Tc2[i][1]]
            
        
        for i in range(len(xs)):
            xs[i] += -Chc2[0][0]
            ys[i] += -Chc2[0][1]
            
        x = []
        y = []
        
        for i in range(len(Atoms)):
            x += [Atoms[i][0]]
            y += [Atoms[i][1]]
            
        plt.plot(xs,ys,'ob')
        plt.plot(x,y, '.r')
        plt.grid()
        plt.show()
    
    #roll up atoms to get nanotube
    AtomsR = []
    thetaCh = None
    thetap = None

    vCh = Chc[1]-Chc[0]
    mCh = mag(Chc[0],Chc[1])
    #RCh = mCh/(2*np.pi)

    xCh = vCh[0]
    yCh = vCh[1]
    thetaCh = atan2(yCh,xCh)
    
    zmax = max(atom1[2],atom2[2])
    #zmin = min(atom1[2],atom2[2])
    #zdif = zmax-zmin
    for i in range(len(Atoms)):
        xp = Atoms[i][0]
        yp = Atoms[i][1]
        zp = Atoms[i][2]
    
            
        thetap = atan2(yp,xp)
        theta = thetap - thetaCh

        Vpoint = [xp,yp]
        Vmag = np.linalg.norm(Vpoint)
        alpha = Vmag*cos(theta)/R
        
        #we are taking into consideration the buckling value
        if abs(zp-zmax) < 1e-2:
            
            z = Vmag*sin(theta) 
            x = R*cos(alpha)
            y = R*sin(alpha)
        else:
            
            z = Vmag*sin(theta) 
            x = 0.9*R*cos(alpha)
            y = 0.9*R*sin(alpha)
        
        if z>np.linalg.norm(T):
            pass
        else: 
            AtomsR += [(x-25,y-25,z)]

    #define unit cell 
    unit_Cell = [[ 0.0000000000,    0.0000000000,    0.0000000000],
                [0.0000000000,   0.0000000000,    0.0000000000],
                [0.0000000000,    0.0000000000, unitCell]]

    return AtomsR,unit_Cell

if __name__=="__main__":
    """
    Example: Blue-Phosphorene nanotubes with n=14 and m = 14
    """
    app = 1.88
    a2 = ((3/2.)*app,-np.sqrt(3)/2. * app )
    a1 = ((3/2.)*app,np.sqrt(3)/2. * app )

    #initial atoms 

    atom1 = (0.0,0.0,5.0)
    atom2 = (app,0.0,6.3)
    n = 14
    m = 14
    atomsR,unitCell = make_nanotube(app,a1,a2,atom1,atom2,n,m,False)
    writexyz(atomsR,'BPNT1414')
 